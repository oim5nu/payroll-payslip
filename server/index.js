const app = require('./server');

// app().catch(console.error);
app.listen(3001, () => console.log('Server start at PORT 3001'));

module.exports = app;