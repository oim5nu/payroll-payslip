const handlePost = (db) => async (req, res, next) => {
  //console.log('req.body', req.body);
  const {
      name, payDate, frequency, annualIncome, gross,
      tax, net, superannuation, pay
  } = req.body;

  // Could use more strict validation here
  if (!name || !payDate || !pay) {
    next(new Error('no valid name, date or payment'));
  }

  const foundPayslip = await db.get('payslips')
    .filter({name, payDate})
    .value();
  //console.log('foundPayslip', foundPayslip);
  if (foundPayslip[0]) {
    return next(new Error('Existing payslip found'));
  }

  const _payslip = {
    name, payDate, frequency, annualIncome, gross,
    tax, net, superannuation, pay  
  };

  await db.get('payslips').push(_payslip).write();

  const payslip_ = await db.get('payslips')
    .filter({name, payDate})
    .value();
  
  if ( payslip_[0] ) {
    res.json({ message: 'Record received', payload: {...payslip_[0]}});
  } else {
    next(new Error('Record not received'))
  }
};

module.exports = {
  handlePost
};