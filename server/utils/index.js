// Wrapper function to allow writing async/await syntax in express
const asyncMiddleware = fn =>
  (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next);
  };

const logErrors = (err, req, res, next) => {
  console.error(err.stack);
  next(err);
};

const clientErrorHandler = (err, req, res, next) => {
  if (req.xhr) {
    res.status(500).json({ error: 'Something failed!'});
  } else {
    next(err);
  }
};

const error404Handler = (err, req, res, next) => {
  let errMessage = new Error('Invalid Operation!');
  errMessage.status = 404;
  next(errMessage);
};

const finalErrorHandler = (err, req, res, next) => {
  res.status(500);
  res.json({ error: err.message });
};

module.exports = {
  logErrors,
  clientErrorHandler,
  error404Handler,
  finalErrorHandler,
  asyncMiddleware
};