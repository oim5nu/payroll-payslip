require('dotenv').config();
const path = require('path');


module.exports = {
  development: {
    url: path.join(__dirname, '..', 'data', process.env.DEV_DATABASE_NAME)
  },
  test: {
    url: path.join(__dirname, '..', 'data', process.env.TEST_DATABASE_NAME)
  },
  production: {
    url: process.env.PRODUCTION_DATABASE_URL
  }
};