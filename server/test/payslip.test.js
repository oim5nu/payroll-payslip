const server = require("../server");
const chai = require('chai');
const chaiHttp = require('chai-http');
const { initialiseDatabase, clearDatabase } = require('./common');

const { expect } = chai;
chai.use(chaiHttp);

let db;

describe("Payslip", () => {
  before( async () => {
    db = await initialiseDatabase();
  });

  it("should persist record if name, payDate or pay are valid", done => {
    chai
      .request(server)
      .post("/api/v1/payslip")
      .send({ 
        name: 'Sarah2',
        payDate: '2012-12-12',
        pay: 200.00
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.payload.name).to.equals("Sarah2");
        expect(res.body.payload.payDate).to.equals("2012-12-12");
        expect(+res.body.payload.pay).to.equals(200.00);
        done();
      });
  });

  it("should reject persisting payslip record, if duplicate record of name and payDate is found ", done => {
    chai
      .request(server)
      .post("/api/v1/payslip")
      .send({ 
        name: 'Sarah2',
        payDate: '2012-12-12',
        pay: 300.00  //missing valid field here
      })
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res.body.error).to.equals("Invalid Operation!");
        done();
      });
  });   

  it("should reject persisting payslip record, if name, payDate or pay are invalid", done => {
    chai
      .request(server)
      .post("/api/v1/payslip")
      .send({ 
        name: 'Sarah2',
        payDate: '2012-12-12',
        //pay: 200.00  //missing valid field here
      })
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res.body.error).to.equals("Invalid Operation!");
        done();
      });
  });     

  after( async () => {
    await clearDatabase(db);
  });
});