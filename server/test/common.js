process.env.NODE_ENV="test";
const lowdb = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');

require('dotenv').config();
const databasePath = require('../config/config')[process.env.NODE_ENV]['url'];
console.log('databasePath in tests/common', databasePath);
const adapter = new FileAsync(databasePath);

const initialiseDatabase = async () => {
  const db = await lowdb(adapter);
  await db.remove().defaults({ payslips: [] }).write();
  return db;
};

const clearDatabase = async (db) => {
  await db.get('payslips').remove().write();
};

module.exports = {
  initialiseDatabase,
  clearDatabase
};