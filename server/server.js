'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');
const app = express();
const cors = require('cors');
const path = require('path');
const lowdb = require('lowdb');
const FileAsync = require('lowdb/adapters/FileAsync');
require('dotenv').config();
const databasePath = require('./config/config')[process.env.NODE_ENV || 'development']['url'];

const adapter = new FileAsync(databasePath); 

const {
  logErrors,
  clientErrorHandler,
  error404Handler,
  finalErrorHandler,
  asyncMiddleware
} = require('./utils');

const payslip = require('./controllers/payslip');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded( { extended: false} ));

// parse application/json
app.use(bodyParser.json());

// log access data
app.use(morgan(':date[clf :remote-addr, :referrer, :url :response-time ms', {
  stream: fs.createWriteStream(path.join(__dirname, 'log/access.log'), {flags: 'a'})
}));
app.use(cors());

lowdb(adapter)
  .then(db => {

    app.post('/api/v1/payslip', asyncMiddleware(payslip.handlePost(db)));
    
    db.defaults({ payslips: [] }).write();

    app.use(logErrors);
    app.use(clientErrorHandler);
    app.use(error404Handler);
    app.use(finalErrorHandler);  
  })
  .catch(console.error);

module.exports = app;