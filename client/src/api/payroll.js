import axios from 'axios';

class payrollApi {
  static postPayslip(values) {
    return axios({
      method: 'post',
      url: '/api/v1/payslip',
      data: {
        ...values
      }
    });
  }
}

export default payrollApi;