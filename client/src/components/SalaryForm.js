import React, { Fragment, useEffect } from 'react';
import { Form, Grid, Button, Message } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { generateAction } from '../redux/actions/payslip';
import { getPayslipGenerated } from '../redux/selectors/payslip';
import useForm from 'react-hook-form';


const SalaryForm = (props) => {
  useEffect(() => {
    register({ name: "firstname" }, {validate: {
      requiredWithMaxLen: value => ((!!value && String(value || '').length <= 80)  || 'Firstname: Required and length smaller than 80'),
    }});
    register({ name: "lastname" }, {validate: {
      requiredWithMaxLen: value => ((!!value && String(value || '').length <= 100) || 'Lastname: Required and length smaller than 100'),
    }});
    register({ name: "annualIncome" }, { validate: {
      mustBeInteger: value => (/^\d+$/g.test(value) || 'Salary: Must be integer'),
    }});
    register({ name: "superRate" }, { validate: {
      mustBeInteger: value => ((/^\d+$/g.test(value) && +value <= 100) || 'Super rate: Must be integer smaller than 100'),
    }});
  }, []);
  const { register, errors, handleSubmit, setValue, triggerValidation } = useForm();  
  const onSubmit = (data, e) => {
    e.preventDefault();
    props.actions.generateAction(data);
  };

  return (
    <Fragment>
      { errors.firstname ?                     
          <Message
            error
            content={errors.firstname.message}
          /> : null }
      { errors.lastname ?                     
        <Message
          error
          content={errors.lastname.message}
        /> : null }   
      { errors.annualIncome ?                     
        <Message
          error
          content={errors.annualIncome.message}
        /> : null } 
      { errors.superRate ?                     
        <Message
          error
          content={errors.superRate.message}
        /> : null }                       
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Grid columns={1} >
          <Grid.Row>
            <Grid.Column>
                <Form.Group widths='equal'>
                    <Form.Input 
                      name="firstname"
                      fluid
                      placeholder="Firstname"
                      defaultValue={''}
                      onChange={async (e, {name, value}) => {
                        setValue(name, value);
                        await triggerValidation({name})
                      }}  
                      error={errors.firstname ? true : false}                   
                    />
                    <Form.Input 
                      name="lastname"
                      fluid
                      placeholder="Lastname"
                      defaultValue={''}
                      onChange={async (e, {name, value}) => {
                        setValue(name, value);
                        await triggerValidation({name})
                      }}  
                      error={errors.lastname ? true : false} 
                    />
                </Form.Group>
            </Grid.Column>
            <Grid.Column>
              <Form.Group widths='equal'>
                <Form.Input 
                  name="annualIncome" 
                  fluid
                  placeholder="Annual Salary" 
                  defaultValue={0}
                  onChange={async (e, {name, value}) => {
                    setValue(name, value);
                    await triggerValidation({name})
                  }}  
                  error={errors.annualIncome ? true : false} 
                />
                <Form.Input 
                  name="superRate"
                  fluid
                  placeholder="Super Rate"
                  defaultValue={0}
                  onChange={async (e, {name, value}) => {
                    setValue(name, value);
                    await triggerValidation({name})
                  }}  
                  error={errors.superRate ? true : false} 
                />      
              </Form.Group>                
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
          </Grid.Row>
        </Grid>
        <Form.Field>
          <Button 
            type="submit" 
            disabled={Object.keys(errors).length > 0 ? true : false }
            floated='right'
            primary
            color="blue"
          >Generate Payslip
          </Button>
        </Form.Field>
      </Form>
    </Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    generated: getPayslipGenerated(state)
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(
      { 
        generateAction
      },
      dispatch
    )
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SalaryForm);