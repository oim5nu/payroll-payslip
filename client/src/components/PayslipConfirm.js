import React, { Fragment, useState } from 'react';
import { Header, Form, Table, Button, Message } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { submitAction, resetAction } from '../redux/actions/payslip';
import { getPayslipGenerated, getPayslipValues, getPayslipMessage, getPayslipError } from '../redux/selectors/payslip';
import dayjs from 'dayjs';
import { numberWithCommas } from '../utils/common';


const PayslipConfirm = (props) => {
  const keyDescription = {
    name: "Name",
    payDate: "Pay Date",
    frequency: "Pay Frequency",
    annualIncome: "Annual Income",
    gross: "Gross Income",
    tax: "Income Tax",
    net: "Net Income",
    superannuation: "Super",
    pay: "Pay"
  };
  const onMessageDismiss = () => {
    props.actions.resetAction();
  }
  const onSubmit = (e) => {
    e.preventDefault();
    props.actions.submitAction({...props.values});
  };
  console.log('props.message', props.message);
  return (
    <Fragment>
      <Header as='h1'>Payslip</Header>
      <Header as='h2'>{props.values['name']}</Header>
      {props.message ? <Message attached success onDismiss={onMessageDismiss} header="Message from Server" content={props.message} /> : null}
      {props.error ? <Message attached error onDismiss={onMessageDismiss} header="Error from Server" content={String(props.error)} /> : null}
      <Form onSubmit={onSubmit}>
        <Form.Field>
          <Table striped>
            <Table.Header>
            </Table.Header>
            <Table.Body>
              {Object.keys(props.values).map( key => { 
                if (key === 'name') {
                  return null;
                } else if (key === 'payDate') {
                  return (
                    <Table.Row key={key}>
                      <Table.Cell>{keyDescription[key]}</Table.Cell>
                      <Table.Cell>{ dayjs(props.values[key]).format("DD MMMM YYYY")}</Table.Cell>
                    </Table.Row>
                  );                
                }
                else {
                  return (
                    <Table.Row key={key}>
                      <Table.Cell>{keyDescription[key]}</Table.Cell>
                      <Table.Cell>{ isNaN(+props.values[key]) ? props.values[key] : `$${numberWithCommas((+props.values[key]).toFixed(2))}`}</Table.Cell>
                    </Table.Row>
                  );
                }})
              }
            </Table.Body>
          </Table>
        </Form.Field>
        <Form.Field>
          <Button type="submit"             
            floated='right'
            primary
            color="blue">
            Pay
          </Button>
        </Form.Field>
      </Form>
    </Fragment>
  );
}

const mapStateToProps = (state) => {
  return {
    error: getPayslipError(state),
    message: getPayslipMessage(state),
    generated: getPayslipGenerated(state),
    values: getPayslipValues(state)
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(
      { 
        submitAction,
        resetAction,
      },
      dispatch
    )
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PayslipConfirm);