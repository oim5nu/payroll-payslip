import React from 'react';
import FlexDirection from './components/FlexDirection';
import { Container } from 'semantic-ui-react';
import { connect } from "react-redux";
import { getPayslipName, getPayslipGenerated } from './redux/selectors/payslip';
import SalaryForm from './components/SalaryForm';
import PayslipConfirm from './components/PayslipConfirm';


const App = (props) => {
  const { generated } = props;
  return (
    <FlexDirection>
      <Container style={{ marginTop: "5em" }}>
      { !!generated ? (<PayslipConfirm />) : (<SalaryForm />) }
      </Container>
    </FlexDirection>
  );
};

function mapStateToProps(state) {
  return {
    generated: getPayslipGenerated(state),
    name: getPayslipName(state)
  }
}

export default connect(mapStateToProps, null)(App);