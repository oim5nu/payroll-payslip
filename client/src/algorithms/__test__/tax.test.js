import { TAX_TABLE, DEFAULT_TAX_YEAR} from '../../utils/constants'; 
import { getTaxRate, calculateTax } from '../tax';

describe("Algorithms of tax factories", () => {
  const currentYear = (new Date()).getFullYear();
  //console.log('TAX_TABLE', TAX_TABLE);
  const taxTable = TAX_TABLE[String(currentYear)] || TAX_TABLE[String(DEFAULT_TAX_YEAR)] ;
  const annualIncome = 60050;
  describe('getTaxRate', () => {
    const expected = { lower: 37001, upper: 80000, base: 3572, plusRate: 0.325 };
    it("should return tax rate object as expected", () => {
      const actual = getTaxRate(taxTable, annualIncome);
      expect(actual).toEqual(expected);
    });
  });
  describe('calculateTax', () => {
    const expected = { gross: 5004, tax: 922, net: 4082, superannuation: 450, pay: 3632 };
    const superRate = 9;
    it("should return tax rate object as expected", () => {
      const actual = calculateTax(taxTable, annualIncome, superRate);
      expect(actual).toEqual(expected);
    });
  });
});