
export function getTaxRate(taxTable=[], annualIncome=0) {
  let taxRate;
  try {
      taxRate = taxTable.find(obj => (
      +annualIncome > +obj.lower && +annualIncome <= +obj.upper 
    ));
  } catch(error) {
    console.log(error);
  } 
  return taxRate;
}

export function calculateTax(taxTable=[], annualIncome=0, superRate=0) {
  try {
    const gross = Math.round(annualIncome / 12);
    const taxRateObj = getTaxRate(taxTable, annualIncome);
    const { lower, base, plusRate } = taxRateObj;
    const tax = Math.round(( base + (annualIncome - lower) * plusRate ) / 12);
    const net = gross - tax;
    const superannuation = Math.round(gross * superRate / 100.00);
    const pay = net - superannuation;
    return {
      gross,
      tax,
      net,
      superannuation,
      pay
    };
  } catch(error) {
    console.log(error);
    return {};
  }
}
