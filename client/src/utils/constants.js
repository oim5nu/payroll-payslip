export const TAX_TABLE = {
  '2012': [
    {lower: 0, upper: 18200, base: 0, plusRate: 0 },
    {lower: 18201, upper: 37000, base: 0, plusRate: 0.19 },
    {lower: 37001, upper: 80000, base: 3572, plusRate: 0.325 },
    {lower: 80001, upper: 180000, base: 17547, plusRate: 0.37 },
    {lower: 180001, upper: Infinity, base: 54547, plusRate: 0.45 },
  ]
};

export const DEFAULT_TAX_YEAR = 2012;

export const SUPERANNUATION_TABLE = {
  '2012': 0.09
};