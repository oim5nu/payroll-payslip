import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from "redux-devtools-extension";

import reducers from "./reducers/index";
import { default as middlewares, sagaMiddleware } from "./middleware";
import rootSaga from "./sagas/index";

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(...middlewares))
);

sagaMiddleware.run(rootSaga);

export default store;