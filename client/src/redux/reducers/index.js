import { combineReducers } from 'redux';
import { payslip } from './payslip';

const reducers = combineReducers({
  payslip
});

export default reducers;
