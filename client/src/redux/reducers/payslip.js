import produce from 'immer';
import { types } from '../actions/actionTypes';
import { capitaliseFirstLetter } from '../../utils/common';
import { calculateTax } from '../../algorithms/tax';
import { TAX_TABLE, DEFAULT_TAX_YEAR } from '../../utils/constants';
import dayjs from 'dayjs';

const initialState = {
  generated: false,
  submitting: false,
  successful: false,
  error: null,
  message: null,
  values: {
    name: null,
    payDate: null,
    frequency: '',
    annualIncome: 0.00,
    gross: 0.00,
    tax: 0.00,
    net: 0.00,
    superannuation: 0.00,
    pay: 0.00
  },
  // name: null,
  // payDate: null,
  // frequency: '',
  // annualIncome: 0.00,
  // gross: 0.00,
  // tax: 0.00,
  // net: 0.00,
  // superannuation: 0.00,
  // pay: 0.00
};

export const payslip = (state = initialState, action = {}) => {
  return produce(state, draft => {
    switch (action.type) {
      case types.PAYSLIP_RESET:
        draft.generated = false;
        draft.submitting = false;
        draft.error = null;
        draft.message = null;
        draft.successful = false;
        draft.values.name = '';
        draft.values.payDate = '';
        draft.values.frequency = '';
        draft.values.annualIncome = 0;
        draft.values.gross = 0;
        draft.values.tax = 0;
        draft.values.net = 0;
        draft.values.superannuation = 0;
        draft.values.pay = 0;        
        break;
      case types.PAYSLIP_GENERATE:
        const currentDate = new Date();
        const currentYear = currentDate.getFullYear(); 
        const taxTable = TAX_TABLE[String(currentYear)] || TAX_TABLE[String(DEFAULT_TAX_YEAR)];

        const _payslip = calculateTax(taxTable, +action.payload.annualIncome, +action.payload.superRate);
        draft.generated = true;
        draft.values.name = `${capitaliseFirstLetter(action.payload.firstname)} ${capitaliseFirstLetter(action.payload.lastname)}`; 
        draft.values.payDate = dayjs().format("YYYY-MM-DD");
        draft.values.frequency = 'Monthly';
        draft.values.annualIncome = +action.payload.annualIncome;
        draft.values.gross = +_payslip.gross;
        draft.values.tax = +_payslip.tax;
        draft.values.net = +_payslip.net;
        draft.values.superannuation = +_payslip.superannuation;
        draft.values.pay = +_payslip.pay;
        break;
      case types.PAYSLIP_SUBMIT:
        draft.submitting = true;
        draft.error = null;
        draft.message = null;
        draft.successful = false;
        draft.values.name = action.payload.name;
        draft.values.payDate = action.payload.payDate;
        draft.values.frequency = action.payload.frequency;
        draft.values.annualIncome = action.payload.annualIncome;
        draft.values.gross = action.payload.gross;
        draft.values.tax = action.payload.tax;
        draft.values.net = action.payload.net;
        draft.values.superannuation = action.payload.superannuation;
        draft.values.pay = action.payload.pay;
        break;
      case types.PAYSLIP_SUBMIT_SUCCESS:
        draft.submitting = false;
        draft.error = null;
        draft.message = action.payload.message;
        draft.successful = true;
        //draft.generated = false;
        break;
      case types.PAYSLIP_SUBMIT_FAILURE:
        draft.submitting = false;
        draft.error = action.payload.error;
        draft.message = action.payload.message;
        draft.successful = false;
        break;
      default:;
    }
  });
};