import { takeLatest, all } from 'redux-saga/effects';
import { types } from '../actions/actionTypes';
import { workerPayslipSubmitSaga } from './payslip';

function* watchAll() {
  yield all([takeLatest(types.PAYSLIP_SUBMIT, workerPayslipSubmitSaga)]);
}

export default watchAll;