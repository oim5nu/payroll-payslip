import { call, put, select } from "redux-saga/effects";
import { getPayslipValues } from "../selectors/payslip";
import { submitSuccessAction, submitFailureAction } from "../actions/payslip";

import PayrollApi from "../../api/payroll";
const postPayslip = PayrollApi.postPayslip;

function* workerPayslipSubmitSaga() {
  try {
    const values = yield select(getPayslipValues);
    const response = yield call(postPayslip, values);

    //console.log("workerPayslipSubmitSaga response", response);
    const { data: { message } } = response;
    yield put(submitSuccessAction(message));
  } catch (error) {
    console.log("workerPayslipSubmitSaga", error);
    yield put(submitFailureAction(error));
  }
}

export { workerPayslipSubmitSaga };