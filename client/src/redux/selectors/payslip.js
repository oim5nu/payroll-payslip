export const getPayslipName = state => state.payslip.name;
export const getPayslipPayDate = state => state.payslip.payDate;
export const getPayslipFrequency = state => state.payslip.frequency;
export const getPayslipAnnualIncome = state => state.payslip.annualIncome;
export const getPayslipGross = state => state.payslip.gross;
export const getPayslipTax = state => state.payslip.tax;
export const getPayslipNet = state => state.payslip.net;
export const getPayslipSuperAnnuation = state => state.payslip.superannuation;
export const getPayslipPay = state => state.payslip.pay;

export const getPayslipSubmitting = state => state.payslip.submitting;
export const getPayslipError = state => state.payslip.error;
export const getPayslipMessage = state => state.payslip.message;
export const getPayslipSuccessful = state => state.payslip.successful;
export const getPayslipGenerated = state => state.payslip.generated;

export const getPayslipValues = state => state.payslip.values;
