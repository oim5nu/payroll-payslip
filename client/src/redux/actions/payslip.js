import { types } from './actionTypes';

export function resetAction() {
  return {
    type: types.PAYSLIP_RESET
  };
}

export function generateAction({
  firstname, lastname, annualIncome, superRate
  }) {
  return {
    type: types.PAYSLIP_GENERATE,
    payload: { firstname, lastname, annualIncome, superRate }
  };
}

export function submitAction({
  name, payDate, frequency, annualIncome, gross,
  tax, net, superannuation, pay
  }) {
  return {
    type: types.PAYSLIP_SUBMIT,
    payload: { name, payDate, frequency, annualIncome, gross,
      tax, net, superannuation, pay }
  };
}

export function submitSuccessAction(message) {
  return {
    type: types.PAYSLIP_SUBMIT_SUCCESS,
    payload: { message }
  };
}

export function submitFailureAction(error, message) {
  return {
    type: types.PAYSLIP_SUBMIT_FAILURE,
    payload: { error, message }
  };
}
