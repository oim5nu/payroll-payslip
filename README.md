# HR PAYROLL SYSTEM - Payslip Solution

## Assumptions

This challenge is to demonstrate ideas and usages of skills for development and testing rather than finish a production.

## Frontend

### Frameworks: 
React - Good for components reusage, reasonable performance, improved function component features like hooks is more declarative and help developer focus on business logic rather than application running logic

Redux - Global state management library. It allows different components sharing or relying on global state.

Redux-saga - Easy to build and test async action even the scenarios is more complated

Semantic-ui-react - Popular UI kid for react, good document tation.

react-hook-form - New react form valiation library which support react hooks and is more declarative 

immer - Great library for deep copying state, much more easier for use than immutablejs 

reselect - Have not used it, though installed. I built simple seletors myself. Maybe it is used for complex senarios

react-actions - action creators for redux. Have not used it yet. So far I built them myself.

## Backend

### Frameworks

express.js - Popular node.js server library, personally I prefer koa but it's quite new when I used it, worrying about lacking middlewares I needed

dotenv - Environment variables setting libary, good to keep the environment variables (secret keys) in .env, and load it in runtime rather than hard-code in application

morgan - logger for express

lowerdb - lightweight local JSON based database support Node.js Eletron and browser. Similar to lokijs, but it provides promise-like API. It's good for POC or quick demo and lightweight usage. Iffor  going real-world option, SQLite would be much more stable, and industry proved. Production choice would be Mongodb / PostgreSQL.

## Install & Usage Instructions

### Requirements: git & Node.js > 8 under Windows 7/8/10, Mac OSX or Linux (I have not tried linux yet)

### Steps for Installation
1. git clone https://bitbucket.org/oim5nu/payroll-payslip.git
2. cd payroll-payslip
3. cd server
4. yarn install
4. mv .env.example .env #this is for mac or linux, or windows, ren .env.example .env 
5. edit .env, set it as
DEV_DATABASE_NAME=payroll.json 
TEST_DATABASE_NAME=payroll_test.json 
6. yarn start
7. open another terminal
8. cd /somepath/payroll-payslip/client
9. yarn install
10. yarn start
11. open brower, type in http://localhost:3000

### Steps for testing (mainly backend test cases)
yarn test
